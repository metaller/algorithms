#include "vector"
#include "sorts.h"
#include "tests.h"

void test_QuickSort() {
    { /* test_1 */
        int size = 10;
        int* ar = new int[size]{-4, -23, 12, 0, 5, 3, 87, 65, -1, 3};
        quickSort(&ar[0], size);
        delete [] ar;
    }
    { /* test_2 */
        int size = 7;
        int* ar = new int[size]{-20, 2, 0, 11, 4, 7, 55};
        quickSort(&ar[0], size);
        delete [] ar;
    }
    { /* test_3 */
        int size = 5;
        int* ar = new int[size]{-9, 5, 1, 0, 32};
        quickSort(&ar[0], size);
        delete [] ar;
    }
    { /* test_4 */
        int size = 5;
        int* ar = new int[size]{6, 3, 12, -8, 34};
        quickSort(&ar[0], size);
        delete [] ar;
    }
}
