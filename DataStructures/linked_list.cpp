#include <iostream>

template<class T>
struct Node {
	T value;
    	Node* next_node;
//  ----------------
    	Node(T _value = T(), Node* _next_node = nullptr) : value(_value), next_node(_next_node) {}
	~Node() {}
};

template<class T>
class LinkedList {
	Node<T>* head;
public:
	LinkedList() { head = nullptr; }
	~LinkedList() {}
//  ----------------
    	bool is_empty();
	void show(LinkedList<T>* list);
	int size();
    	void push_back(T value);
    	void pop_back();
};

template<typename T>
bool LinkedList<T>::is_empty() {
    	return head == nullptr;
} 

template<typename T>
int LinkedList<T>::size() {

}

template<typename T>
void LinkedList<T>::show(LinkedList<T>* list) {
	Node<T>* temp = list->head; 
	Node<T>* temp2;
	for(int i = 0; temp != nullptr; ++i) {
		std::cout << temp->value << std::endl;
		temp2 = temp;
		temp = temp2->next_node;
	}
}

template<typename T>
void LinkedList<T>::push_back(T value) {
	Node<T>* temp = new Node<T>(value);
	Node<T>* cur;
    	if(is_empty()) {
     		head = temp;
      		return;
	}
	if(head->next_node == nullptr) {
		head->next_node = temp;
		cur = temp;
		return;
	}
	cur->next_node = temp;
	cur = temp;
}

template<typename T>
void LinkedList<T>::pop_back() {

}

int main() {
	LinkedList<int> list;
	list.push_back(3414);
	list.push_back(12);
	list.push_back(72);
	list.push_back(4);
	list.push_back(124);
	list.push_back(0);
	list.push_back(123);
	list.push_back(-5);
	list.show(&list);
	return 0;
}
