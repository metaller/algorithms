#include <iostream>
#include <vector>

void binsearch(int* ar, int size, int key) {
	if(size == 0) { return; }
	int center = size / 2;
	if(key == ar[center]) { return; }
	if(key <= ar[center] && size != 0) {
		size /= 2;
		binsearch(&ar[0], size, key);
	}
	if(key > ar[center] && size != 0) {
		size /= 2;
	       	binsearch(&ar[size], size, key);
	}
}

void linsearch(int* ar, int size, int key) {
	if(size == 0) { return; }
	for(int i = 0; i < size; ++i) {
		if(ar[i] == key) {
			break;
		}
	}
}

int main() {
	int ar[9]{1,2,3,4,5,7,8,9,10};
	binsearch(&ar[0], 9, 4);
	return 0;
}
